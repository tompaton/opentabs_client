from collections import namedtuple

import requests
from requests.auth import HTTPBasicAuth


class PostError(Exception):
    pass


class Opentabs:

    URL = 'https://opentabs.tompaton.com/'
    Page = namedtuple('Page', 'url_id url title')

    def __init__(self, uid, pwd):
        self._uid = uid
        self._pwd = pwd

    def get_urls(self):
        return [self.Page(**row)
                for row in self._send_request('GET', self.URL).json()]

    def add_url(self, url, title):
        res = self._send_request('POST', self.URL,
                                 data={'page_url': url, 'page_title': title})

        if res.status_code == 200:
            return self.Page(**res.json())

        else:
            raise PostError(res.text)

    def delete_url(self, url_id):
        res = self._send_request('DELETE', self.URL + str(url_id))

        if res.status_code == 200:
            return True

        else:
            raise PostError(res.text)

    def _send_request(self, method, url, **kwargs):  # pragma: no cover
        req = requests.Request(method, url,
                               auth=HTTPBasicAuth(self._uid, self._pwd),
                               headers={'Accept': 'application/json'},
                               **kwargs)
        return requests.Session().send(req.prepare())

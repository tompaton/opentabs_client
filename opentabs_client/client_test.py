from unittest.mock import Mock, patch, call

import pytest

import opentabs_client as client


@pytest.fixture
def tabs():
    return client.Opentabs('uid', 'pwd')


@pytest.fixture
def tabs_json():
    return [{"url_id": 123,
             "title": "Example Page",
             "url": "http://example.com/page.html"},
            {"url_id": 234,
             "title": "Page Two",
             "url": "http://example.net/two.html"}]


def test_get_urls(tabs, tabs_json):
    with patch.object(tabs, '_send_request') as send:
        send.return_value = Mock()
        send.return_value.json.return_value = tabs_json

        assert tabs.get_urls() == [
            client.Opentabs.Page(
                123, 'http://example.com/page.html', 'Example Page'),
            client.Opentabs.Page(
                234, 'http://example.net/two.html', 'Page Two'),
        ]

        send.assert_called_with(
            'GET', 'https://opentabs.tompaton.com/')


def test_add_url(tabs):
    with patch.object(tabs, '_send_request') as send:
        send.return_value = Mock()
        send.return_value.status_code = 200
        send.return_value.json.return_value = {
            'url_id': 345, 'url': 'new-url', 'title': 'new-title'
        }

        assert tabs.add_url('new-url', 'new-title') \
            == client.Opentabs.Page(345, 'new-url', 'new-title')

        assert send.mock_calls == [
            call('POST', 'https://opentabs.tompaton.com/',
                 data={'page_title': 'new-title',
                       'page_url': 'new-url'}),
            call().json(),
        ]


def test_add_url_error(tabs):
    with patch.object(tabs, '_send_request') as send:
        send.return_value = Mock()
        send.return_value.status_code = 500
        send.return_value.text = 'error'

        with pytest.raises(client.PostError) as execinfo:
            tabs.add_url('new-url', 'new-title')

        assert str(execinfo.value) == 'error'

        assert send.mock_calls == [
            call('POST', 'https://opentabs.tompaton.com/',
                 data={'page_title': 'new-title',
                       'page_url': 'new-url'}),
        ]


def test_delete_url(tabs):
    with patch.object(tabs, '_send_request') as send:
        send.return_value = Mock()
        send.return_value.status_code = 200

        assert tabs.delete_url(456)

        assert send.mock_calls == [
            call('DELETE', 'https://opentabs.tompaton.com/456'),
        ]


def test_delete_url_error(tabs):
    with patch.object(tabs, '_send_request') as send:
        send.return_value = Mock()
        send.return_value.status_code = 500
        send.return_value.text = 'error'

        with pytest.raises(client.PostError) as execinfo:
            tabs.delete_url(456)

        assert str(execinfo.value) == 'error'

        assert send.mock_calls == [
            call('DELETE', 'https://opentabs.tompaton.com/456'),
        ]
